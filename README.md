# Blank Theme Moodle

### About
This package provides a blank theme to facilitate cloning of the Clean Theme and develop a new theme.

### Installation (git clone)
    
    git clone https://bitbucket.org/michaelmeneses/theme_blank blank
