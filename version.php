<?php

/**
 * Blank Theme Moodle
 *
 * @package    theme_blank
 * @author     Michael Meneses <michael@michaelmeneses.com.br>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2015062800;
$plugin->requires  = 2014050800;
$plugin->component = 'theme_blank';
$plugin->dependencies = array(
    'theme_bootstrapbase'  => 2014050800,
);
